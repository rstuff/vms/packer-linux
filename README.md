# Packer Linux (templates)

Packer templates for Linux VMs creation as Vagrant boxes.

> Currently only virtualbox and LinuxMint are available.

## Requirements

* [Packer](https://www.packer.io/downloads)
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)

## Current templates

* Linux Mint Cinnamon 20

### Files

* [Preseed / http](http/) : files to automate installation.
* [main\_template.json](main\_template.json) : packer template
* \<os\>-\<gui\>-\<version\>.json : packer template containing variables
* [scripts/](scripts/) : installation scripts

### Variables

Edit _\<os\>-\<gui\>-\<version\>.json_ to set Variables.  

## Build Vagrant Boxes

Build a VM virtualbox with Linux Mint Cinnamon 20.
```
packer build --only=virtualbox-iso -var-file=linuxmint-cinnamon-20.0.json main_template.json
```

Box will be available in _box/\<provider\>/\<vm_name\>-\<version\>.box_

## License

[MIT](LICENSE)

## Authors

* [Jean-François Rey](https://gitlab.com/_jfrey_)

## Contributions

All contributions are welcome.

## Acknowledgments

[ajxb](https://github.com/ajxb/packer-linuxmint) for is work by providing the code on which some of this repository is based.
